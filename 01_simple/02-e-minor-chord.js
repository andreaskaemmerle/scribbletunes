const scribble = require('scribbletune');
let chord = scribble.clip({
	notes: scribble.scale('e', 'minor', 3),
	pattern: 'x-'.repeat(8)
});

scribble.midi(chord, '02-e-minor-chord.mid');
