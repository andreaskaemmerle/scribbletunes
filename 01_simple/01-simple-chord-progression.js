const scribble = require('scribbletune');
let chords = scribble.clip({
	notes: ['F#min', 'C#min', 'Dmaj', 'Bmin', 'Emaj', 'Amaj', 'Dmaj', 'C#min', 'Amaj'],
	pattern: 'x_x_x_--x_xxx_-x'.repeat(8),
	accentMap: 'x---x---x-x---x-',
	sizzle: true
});

scribble.midi(chords, '01-simple-chord-progression.mid');
