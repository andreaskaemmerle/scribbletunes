const scribble = require('scribbletune');
let kick, hats, bass;

// 4/4 kick
kick = scribble.clip({
	notes: ['c2'],
	pattern: 'x--x'.repeat(4)
});

scribble.midi(kick, '03-drum-bass-loop-kick.mid');

// 16 accentuated hats
hats = scribble.clip({
	notes: ['c4'],
	pattern: 'x'.repeat(16),
	accentMap: 'x---x---x-x-x-x-'
});

scribble.midi(hats, '03-drum-bass-loop-hats.mid');

// bass line
bass = scribble.clip({
	notes: scribble.scale('a', 'minor', 2).slice(0, 3),
	pattern: '--x-'.repeat(4),
	shuffle: true
});

scribble.midi(bass, '03-drum-bass-loop-bass.mid');
