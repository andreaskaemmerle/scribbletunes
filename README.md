# scribbletunes
My first scribbletune tunes 🎵 based on [scribbletune](https://github.com/walmik/scribbletune) by [Walmik Deshpande](https://github.com/walmik).

## Run the tunez
To create MIDI files based on provided scribbletunes, just run `node filename` from your directory with [scribbletune](https://github.com/walmik/scribbletune) installed. Import the MIDI file into your favorite music editing software and have fun.
I'm using Apple Logic Pro X.

### Example
_chord-progression.mid_ is generated from _chord-progression.js_ via `node chord-progression.js`.
